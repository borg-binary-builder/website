<?php
function get_binary_files()
{
    $files = [];
    foreach (scandir("./bin") ?? [] as $filename) {
	if (strpos($filename, ".") == 0 || strpos($filename, ".asc") > -1) continue;
	$file = [];
	$file["name"] = $filename;
	$file["created"] = gmdate("Y-m-d\TH:i:s\Z", filemtime("./bin/" . $filename));
	$file["version"] = explode("-", $filename)[1];
	$file["size"] = filesize("./bin/" . $filename);
        $stability = 'stable';
        if (strpos($file["version"], 'a') !== false) {
            $stability = 'alpha';
        } elseif (strpos($file["version"], 'b') !== false) {
            $stability = 'beta';
        }
        $file["stability"] = $stability;

        $files[$filename] = $file;
    }
    krsort($files, SORT_NATURAL);
    return $files;
}

function save_binaries_json($files, $version)
{
    if (empty($files) || empty($version)) {
        return;
    }

    $binaries = [];
    foreach (['armv5', 'armv6', 'armv7', 'arm64'] as $flavor) {
        $binary            = "borg-{$version}-{$flavor}";
        $binaries[$flavor] = [
            'version' => $files[$binary]["version"],
            'size'    => $files[$binary]["size"],
            'sha256'  => $files[$binary]["sha256"],
            'url'     => get_dl_link($flavor, $version),
        ];
    }
    file_put_contents('binaries.json', json_encode($binaries));
}

function human_filesize($bytes, $decimals = 2)
{
    if ($bytes === null) {
        return '';
    }

    $sizes  = 'BKMGTP';
    $factor = floor((strlen($bytes) - 1) / 3) ?: 0;
    $suffix = @$sizes[$factor];
    if ($suffix === 'B') {
        return "{$bytes}B";
    }

    return sprintf("%.{$decimals}f", $bytes / (1024 ** $factor)) . "{$suffix}iB";
}

function get_dl_link($flavor, $version, $file = null)
{
    $base = 'https://borg.bauerj.eu/bin';
    if ($file !== null) {
        return "{$base}/{$file}";
    }
    return "{$base}/borg-{$version}-{$flavor}";
}

// Fetch the clean list of binary files.
$files = get_binary_files();

// Get the latest stable version number, fallback to 1.1.10.
$latest = '1.1.10';
foreach ($files as $file) {
    if ($file["stability"] === 'stable') {
        $stable = $file["version"];
        break;
    }
}

if (isset($_GET['show-unstable'])) {
    $latest = reset($files)["version"];
}
else {
    $latest = $stable;
}


// Save binary details of latest stable version to "binaries.json".
save_binaries_json($files, $stable);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Borg ARM binaries</title>
    <link rel="stylesheet" type="text/css" href="_assets/styles.css">
</head>
<body>
<div id="content">
    <div id="content-header">
        <h1>Borg ARM builds</h1> <br>Currently there are automated builds for three different versions of ARM hardware:

        <div class="flavor">
            <a class="download" href="<?php echo get_dl_link('armv5', $latest); ?>">
                <h2>ARMv5 / ARMv4t</h2><br>
                <b class="download-link">Borg <?php echo $latest; ?><br><br><?php echo human_filesize($files["borg-{$latest}-armv5"]["size"] ?? null); ?></b>
            </a>
            <span class="description">
                This version is built for 32Bit-ARM CPUs without a Floating-Point-Unit (ARM v4t and v5).<br>
                <br>
                A lot of NAS devices are using this type of hardware.<br>
                <code>
                    <b class="command">wget</b> <b class="url"><?php echo get_dl_link('armv5', $latest); ?></b> -O <b class="file">/usr/bin/borg</b><br>
                    <b class="command">chmod</b> a+x <b class="file">/usr/bin/borg</b>
                </code>
            </span>
        </div>

        <div class="flavor">
            <a class="download" href="<?php echo get_dl_link('armv6', $latest); ?>">
                <h2>ARMv7 / ARMv6</h2><br>
                <b class="download-link">Borg <?php echo $latest ?><br><br><?php echo human_filesize($files["borg-{$latest}-armv6"]["size"] ?? null); ?></b>
            </a>
            <span class="description">
                This version is built for 32Bit-ARM CPUs with a Floating-Point-Unit (ARM v6 and v7).<br>
                <br>A lot of modern ARM boards and devices ship with a floating-point unit (FPU).
                This includes Raspberry Pis and some powerful NAS devices.<br>
                <code>
                    <b class="command">wget</b> <b class="url"><?php echo get_dl_link('armv6', $latest); ?></b> -O <b class="file">/usr/bin/borg</b><br>
                    <b class="command">chmod</b> a+x <b class="file">/usr/bin/borg</b>
                </code>
            </span>
        </div>

        <div class="flavor">
            <a class="download" href="<?php echo get_dl_link('arm64', $latest); ?>">
                <h2>ARMv8 (or ARM64)</h2><br>
                <b class="download-link">Borg <?php echo $latest ?><br><br><?php echo human_filesize($files["borg-{$latest}-arm64"]["size"] ?? null); ?></b>
            </a>
            <span class="description">
                This version is built for modern 64Bit-ARM CPUs (ARM v8).<br>
                Examples include modern smartphones or the Pine64.<br>
                <code>
                    <b class="command">wget</b> <b class="url"><?php echo get_dl_link('arm64', $latest); ?></b> -O <b class="file">/usr/bin/borg</b><br>
                    <b class="command">chmod</b> a+x <b class="file">/usr/bin/borg</b>
                </code>
            </span>
        </div>
    </div>

    <h2 id="faq">Frequently Asked Questions</h2>
    <div class="container">
        <h3>Which version should I choose?</h3>
        There is probably only one version that's going to run on your device. Run <code><b class="command">uname</b> -m</code> and choose the version that's named similar to the output.
    </div>
    <div class="container">
        <h3>Which dependencies do I need?</h3>
        These binaries should work on the specified platform without installing any dependencies except for libc.<br> You can find more
        information about how to use them in the <a href="https://borgbackup.readthedocs.io/en/stable/installation.html#standalone-binary">borg documentation</a>.
    </div>
    <div class="container">
        <h3>How can I check the authenticity of the binaries?</h3>
        All binaries are signed with PGP key <a href="/borg-binary-builder.asc"><i>A96A45C9</i></a>.<br>
        <code class="pgp-verify">
            # Import public key:<br>
            <b class="command">gpg</b> --import <b class="file">borg-binary-builder.asc</b><br>
            <br>
            # Verify the downloaded binary with it's signature file:<br>
            <b class="command">gpg</b> --verify <b class="file">&lt;borg-binary.asc&gt;</b> <b class="file">&lt;borg-binary&gt;</b>
        </code>
    </div>
    <div class="container">
        <h3>Where can I submit bug reports or feature requests?</h3>
        Please open an issue <a href="https://gitlab.com/borg-binary-builder/borg-binaries">here</a>.
    </div>
    <div class="container">
        <h3>Can I use a script to check this website for updates?</h3>
        Yes, but please use <a href="/binaries.json">this JSON representation</a> in stead of this website. Also, please note that excessive polling might get you banned.
    </div>

    <h2 id="archive">Archive</h2>
    <a href="?show-unstable">(show unstable builds)</a>
    <div id="view" class="view-details">
        <ul id="items" class="clearfix">
            <li class="header">
                <a class="label"><span class="l10n-name">Name</span></a>
                <a class="date descending"><span class="l10n-lastModified">Changed</span></a>
                <a class="size"><span class="l10n-size">Size</span></a>
            </li>
<?php foreach ($files as $file) : ?>
<?php
if ($file["stability"] !== 'stable' && !isset($_GET['show-unstable'])) {
    continue;
}
?>
            <li class="item file stability-<?php echo $file["stability"]; ?>">
                <a href="<?php echo get_dl_link(null, null, $file["name"]); ?>">
                    <span class="label" title="<?php echo $file["name"]; ?>"><?php echo $file["name"]; ?></span>
                    <span class="date" data-time="<?php echo $file["created"]; ?>"><?php echo $file["created"]; ?></span>
                    <span class="size" data-bytes="<?php echo $file["size"]; ?>"><?php echo human_filesize($file["size"]) ?></span>
                </a>
            </li>
<?php endforeach; ?>
        </ul>
    </div>
</div>
</body>
</html>
